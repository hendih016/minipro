$(document).ready(function() {

	$("#btntmbh").click(function() {
		$.ajax({
			url: "http://localhost/addhubungan",
			type: "GET",
			dataType: "html",
			success: function(hasil) {
				$("#modalHubungan").modal('show')
				$(".isiModalHubungan").html(hasil)
			}
		})
		return false
	})

	$("#inBloodCari").keyup(function() {
		let cari = $(this).val()
		list(cari)
	})
	
	list()

	function list(isi) {
		let alamat = ""
		if (isi == undefined || isi == "" || isi== " ") {
			alamat = "http://localhost:81/api/relation/list"
		}
		else {
			alamat = "http://localhost:81/api/relation/listbyname/" + isi
		}

		$.ajax({
			url: alamat,
			type: "GET",
			success: function(hasil) {
				let txt = ""
				for (i = 0; i < hasil.length; i++) {
					txt += `<tr >`
					txt += `<td>${hasil[i].name}</td>`
					txt += "<td><button class='btn btn-warning btnEdit' name='" + hasil[i].id + "' ><i class='fas fa-pen-to-square'></i></button>  <button class='btn btn-danger btndelete' name='" + hasil[i].id + "' ><i class='fas fa-trash'></i></button></td>"
					txt += `</tr>`
				}
				$("#tb2").empty()
				$("#tb2").append(txt)

				$(".btnEdit").click(function() {
					let id = $(this).attr("name")
					sessionStorage.setItem("id", id)

					$.ajax({
						url: "http://localhost/edithubungan",
						type: "GET",
						dataType: "html",
						success: function(hasil) {
							$("#modalHubungan").modal('show')
							$(".isiModalHubungan").html(hasil)
						}
					})
					return false

				})

				$(".btndelete").click(function() {
					let idod = $(this).attr("name")
					sessionStorage.setItem("id", idod)
					var obj = {}
					obj.id = idod
					var myJson = JSON.stringify(obj)

					swal({
						title: "Are you sure?",
						text: "Data Will be Deleted",
						icon: "warning",
						buttons: true,
						dangerMode: true,
					})

						.then((willDelete) => {
							if (willDelete) {
								$.ajax({
									url: "http://localhost:81/api/relation/deletecr",
									type: "DELETE",
									contentType: "application/json",
									data: myJson,
									success: function(hasil) {
										swal("Delete Berhasil", {
											icon: "success",
										});
										
										list()
									}
								})

							} else {
							}
						});


					return false
				})



			}

		})


	}

	list()

})