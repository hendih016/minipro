$(document).ready(function() {

	list()

	function list() {
		$.ajax({
			url: "http://localhost:81/api/levellokasi/list",
			type: "GET",
			success: function(hasil) {
				let txt = ""
				for (i = 0; i < hasil.length; i++) {
					txt += "<tr>"
					txt += "<td>" + hasil[i].name + "</td>"
					txt += "<td>" + hasil[i].abbr + "</td>"
					txt += "<td> <input type='button' value='Edit' class='btnEdit btn btn-primary' name='" + hasil[i].id + "'> "
					txt += "<input type='button' value='Delete' class='btnDelete btn btn-danger' name='" + hasil[i].id + "'> </td>"
					txt += "</tr>"
				}

				$("#tbll").empty();
				$("#tbll").append(txt)

				$(".btnEdit").click(function() {
					let id = $(this).attr("name");
					sessionStorage.setItem("id", id);
					$.ajax({
						url: "http://localhost/lokasileveledit",
						type: "GET",
						dataType: "html",
						success: function(hasil) {
							$("#modalVariants").modal('show')
							$(".isiModalVariants").html(hasil)
						}
					})
					return false
				})
			}
		})
	}
})