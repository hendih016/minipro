
levlokById()

function levlokById() {
	let id = sessionStorage.getItem("id")
	$.ajax({
		url: "http://localhost:81/api/levellokasi/LevLokById/" + id,
		type: "GET",
		success: function(hasil) {
			$("#iName").val(hasil.name)
			$("#iAbbr").val(hasil.abbr)

			$("#btnSimpanEdit").click(function() {
				var obj = {}
				obj.id = hasil.id
				obj.name = $("#iName").val()
				obj.abbr = $("#iAbbr").val()
				obj.c_by = hasil.c_by
				obj.c_on = hasil.c_on
				obj.m_by = 2
				obj.m_on = hasil.m_on
				obj.d_by = hasil.d_by
				obj.d_on = hasil.d_on
				obj.is_del = hasil.is_del
				var myJson = JSON.stringify(obj)

				$.ajax({
					url: "http://localhost:81/api/levellokasi/update",
					type: "PUT",
					contentType: "application/json",
					data: myJson,
					success: function(hasil) {
						alert("Edit berhasil")
						$("#modalVariants").modal('hide')

						list()
					}
				})
			})
		}
	})
}

