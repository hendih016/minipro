
$(".add").click(function() {
	$.ajax({
		url: "http://localhost/addalamat",
		type: "GET",
		datatype: "html",
		success: function(hasil) {
			$("#modaltabalamat").modal('show') //show modal terlebih dahulu
			$(".isimodaltabalamat").html(hasil)
		}
	})
	return false
})

$(".sorting").click(function() {
	let change = $(this)
	if (change.val() == "A-Z") {
		alert($(".sorting").val())
		change.val("Z-A");
	}
	else {
		alert($(".sorting").val())
		change.val("A-Z");
	}
	alert($(".sortby").val())
})

$(".sortby").append("<option value='la'>Label Alamat</option>")
$(".sortby").append("<option value='na'>Nama Penerima</option>")

function kembali() {
	$.ajax({
		url: "http://localhost/viewtabalamat",
		type: "GET",
		datatype: "html",
		success: function(hasil) {
			$("#modaltabalamat").modal('hide')
			$(".isimain").html(hasil)
		}
	})
}

$("#cari").on('input', function() {
	let cari = $(this).val()
	listtab(cari)
})

listtab()
function listtab(isi) {
	let alamat = ""
	if (isi == undefined || isi == "") {
		alamat = "http://localhost:81/api/tabalamat/list"
	} else {
		alamat = "http://localhost:81/api/tabalamat/search/" + isi
	}
	$.ajax({
		url: alamat,
		type: "GET",
		success: function(hasil) {
			//console.log(hasil)
			let txt = ""
			for (i = 0; i < hasil.length; i++) {
				txt += "<tr>"
				txt += "<td><input class='text-primary' type = 'checkbox' id='cbdel" + hasil[i].id + "'></td>"
				txt += "<td><span class='text-primary'>" + hasil[i].label + "</span><br>"
				txt += hasil[i].recipient + ", "
				txt += hasil[i].recipientphonenumber + "<br>"
				txt += hasil[i].address + "</td>"
				txt += "<td><input type='button' value='Ubah' class='btn btn-warning btnupdate' name='" + hasil[i].id + "'> "
				txt += "<input type='button' value='Hapus' class='btn btn-danger btndelete' name='" + hasil[i].id + "'></td>"
				txt += "</tr>"
			}
			$(".tb1").empty()
			$(".tb1").append(txt)

			$(".btnupdate").click(function() {
				let id = $(this).attr("name");
				sessionStorage.setItem("id", id)

				$.ajax({
					url: "http://localhost/updatealamat",
					type: "GET",
					datatype: "html",
					success: function(hasil) {
						$("#modaltabalamat").modal('show')
						$(".isimodaltabalamat").html(hasil)
					}
				})
				return false
			})

			$(".btndelete").click(function() {
				let id = $(this).attr("name")
				sessionStorage.setItem("id", id)
				swal({
					title: "Are you sure?",
					text: "Once deleted, you will not be able to recover this imaginary file!",
					icon: "warning",
					buttons: true,
					dangerMode: true,
				})
					.then((willDelete) => {
						if (willDelete) {
							let id = sessionStorage.getItem("id")
							var obj = {}
							obj.id = id
							var myJson = JSON.stringify(obj)
							$.ajax({
								url: "http://localhost:81/api/tabalamat/delete",
								type: "DELETE",
								contentType: "application/json",
								data: myJson,
								success: function() {
									swal({
										title: "Good job!",
										text: "You clicked the button!",
										icon: "success",
										button: true,
									});
									kembali()
								}
							});
						} else {

						}
					});
			})
		}
	})
}

