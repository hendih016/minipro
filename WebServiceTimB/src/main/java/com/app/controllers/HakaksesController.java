package com.app.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

@Controller
public class HakaksesController {
	@GetMapping("hakakses")
	public String hakakses() {
		return "hakakses/hakakses";
	}
	
	@GetMapping("addhakakses")
	public String addhakakses() {
		return "hakakses/add";
	}
	
	@GetMapping("updatehakakses")
	public String updatehakakses() {
		return "hakakses/update";
	}
	

	@GetMapping("deletehakakes")
	public String deletehakakses() {
		return "hakakses/delete";
	}

}
