package com.app.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class BloodCtrl {

		@GetMapping("blood")
		public String listblood() {
			return "/blood/blood";
		}
		
		@GetMapping("addblood")
		public String addblood() {
			return "/blood/addblood";
		}
		
		@GetMapping("editblood")
		public String editblood() {
			return "/blood/editblood";
		}
}
