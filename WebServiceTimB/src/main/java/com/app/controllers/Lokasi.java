package com.app.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class Lokasi {

	@GetMapping("lokasileveladd")
	public String addLokLev() {
		return "lokasilevel/addlokasilevel.html";
	}	
	
	@GetMapping("lokasileveledit")
	public String listLokLevEdit() {
		return "lokasilevel/editlokasilevel.html";
	}
	
	@GetMapping("lokasilevel")
	public String listLokLev() {
		return "lokasilevel/lokasilevel.html";
	}

	@GetMapping("lokasiadd")
	public String addLok() {
		return "lokasi/addlokasi.html";
	}	
	
	@GetMapping("lokasiedit")
	public String listLokEdit() {
		return "lokasi/editlokasi.html";
	}
	
	@GetMapping("lokasi")
	public String listLok() {
		return "lokasi/lokasi.html";
	}

}
