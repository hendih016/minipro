INSERT INTO m_location_level (id, Name, Abbreviation, Created_by, Created_on, Is_delete) VALUES
	(1,'Provinsi', 'Prov.', 1, NOW(), False),
	(2,'Kota', 'Kota', 1, NOW(), False),
	(3,'Kabupaten', 'Kab.', 1, NOW(), False),
	(4,'Kecamatan', 'Kec.', 1, NOW(), False),
	(5,'Kelurahan', 'Kel.', 1, NOW(), False),
	(6,'Desa', 'Desa', 1, NOW(), False);
	
INSERT INTO m_location (id, name, parent_id, location_level_id, Created_by, Created_on) VALUES
	(1, 'Jakarta Selatan', 4, 2, 1, NOW()),
	(2, 'Pasar Minggu', 1, 4, 1, NOW()),
	(3, 'Kebagusan', 2, 5, 1, NOW()),
	(5, 'Jawa Barat', 0, 1, 1, NOW()),
	(4, 'DKI Jakarta', 0, 1, 1, NOW());

INSERT INTO m_biodata (Fullname , Mobile_phone, Created_by, Created_on) VALUES
	('Hendi', '+628989898989', 1, NOW()),
	('Hidayat', '+628989898989', 1, NOW()),
	('Sama Seperti Sebelumnya', '+628989898989', 1, NOW()),
	('santo','087566363',1,NOW());
	
INSERT INTO m_doctor (Id , biodata_id, str, Created_by, Created_on) VALUES
	('1', '1', 'Gatau isinya apa','1', 'NOW()'),
	('2', '3', 'Gatau isinya apa','1', 'NOW()');
	
INSERT INTO m_medical_facility (Id , name, medical_facility_category_id, location_id, full_address, email, phone_code, phone, fax, Created_by, Created_on) VALUES
	('1', 'RS Mitra', '1', '1', 'Alamatnya dimana ya', 'rsmitra@gmail.com', '+021', '89787878', '+02189787878', '1', 'NOW()'),
	('2', 'RS Persahabatan', '2', '2', 'Alamatnya dimana ya', 'rspersahabata@gmail.com', '+021', '81818181', '+02189787878', '1', 'NOW()');
	
INSERT INTO m_specialization (Id , name, Created_by, Created_on) VALUES
	('1', 'Spesialis Anak','1', 'NOW()'),
	('2', 'Umum', '1', 'NOW()');
	
INSERT INTO t_current_doctor_specialization (Id , doctor_id, specialization_id, Created_by, Created_on) VALUES
	('1', '1', '1', '1', 'NOW()'),
	('2', '2', '1', '1', 'NOW()');
	
INSERT INTO t_doctor_office (Id , doctor_id, medical_facility_id, specialization, Created_by, Created_on) VALUES
	('1', '1', '1', 'Spesialis Anak', '1', 'NOW()'),
	('2', '2', '2', 'Spesialis Anak', '1', 'NOW()');
	
INSERT INTO m_doctor_education (Id , doctor_id, education_level_id, institution_name, major, start_year, end_year, is_last_education, Created_by, Created_on) VALUES
	('1', '1', '1', 'Universitas A', 'Kedokteran A', '2014', '2020', 'true', '1', 'NOW()'),
	('2', '2', '2', 'Universitas B', 'Kedokteran B', '2013', '2019', 'true', '1', 'NOW()');
	
insert into bank (name,va_code)
values
('mandiri','1'),
('bca','2');

insert into role(name,code,created_by,created_on,deleted_by,modified_by)
values
('Admin','Admin_Role','1',now(),0,0),
('Dokter','Dokter_Role','1',now(),0,0),
('Pasien','Pasien_Role','1',now(),0,0),
('Faskes','Faskes_Role','1',now(),0,0);

insert into m_user
(biodata_id,role_id,email,password,login_attempt,is_locked,last_login,created_By,created_On)
values
(1,1,'stevenalbertus80@gmail.com','yo9yakarta',1,true,now(),1,now()),
(3,2,'stevenalbertus85@gmail.com','yo9yakarta',1,true,now(),1,now()),
(4,4,'stevenalbertus89@gmail.com','yo9yakarta',1,true,now(),1,now()),
(2,3,'stevenalbertus81@gmail.com','yo9yakarta',2,true,now(),1,now());


insert into token
(email,user_id,token,expired,is_expired,used_for,created_By,created_On, modified_By, modified_On,deleted_By, deleted_On,is_Delete)
values
('stevenalbertus80@gmail.com',1,'123',now(),false,'asem',1,now(),1,now(),1,now(),false);

insert into m_menu(name,url,parent_id,create_by,create_on,delete_by,modified_by) values
('admin','/admin',1,1,now(),0,0),
('user','/user',1,1,now(),0,0),
('dokter','/dokter',2,1,now(),0,0),
('profile','/profile',3,1,now(),0,0);

insert into m_menu_role(menu_id,role_id,create_by,create_on,delete_by,modified_by) values
(1,1,1,now(),0,0),
(2,1,1,now(),0,0),
(3,1,1,now(),0,0),
(2,3,1,now(),0,0), 
(3,3,1,now(),0,0),
(4,2,1,now(),0,0);
 

insert into m_biodata_address 
(biodata_id,label,recipient,recipient_phone_number,location_id,postal_code,address,created_by,created_on,modified_by,deleted_by)
values 
(1,'Rumah1','Juliantoroo','081284478711',2,'46578','Jl.Anyer Panarukan',2,now(),0,0),
(2,'Rumah2','Juliantorow','081284478712',3,'46578','Jl.Anyer Panarukan',2,now(),0,0),
(3,'Rumah3','Juliantoroq','081284478713',4,'46578','Jl.Anyer Panarukan',2,now(),0,0);
	
