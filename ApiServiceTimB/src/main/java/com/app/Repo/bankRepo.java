package com.app.Repo;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.app.Model.bankModel;




@Transactional
public interface bankRepo extends JpaRepository<bankModel, Long> {
	
	@Query(value="select * from bank where va_code is not null order by id",nativeQuery = true)
	List<bankModel>bankorder();
	
	@Query(value = "select * from bank where id= :id limit 1",nativeQuery = true)
	bankModel bankById(long id);
	
	@Query(value = "select * from bank where lower(name) like lower(concat('%',:nm,'%')) and va_code is not null order by id",nativeQuery = true) //harus di concat kalo ingin huruf kecil
	List<bankModel> bankByNama(String nm);
	
	@Modifying
	@Query(value = "update bank set va_code=null where id= :id  ",nativeQuery = true)
	int deleteBank(long id);
	
}
