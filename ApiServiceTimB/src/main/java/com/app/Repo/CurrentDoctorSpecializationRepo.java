package com.app.Repo;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.app.Model.CurrentDoctorSpecializationModel;

@Transactional
public interface CurrentDoctorSpecializationRepo extends JpaRepository<CurrentDoctorSpecializationModel, Long>{

}
