package com.app.Repo;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.app.Model.userModel;


@Transactional
public interface userRepo extends JpaRepository<userModel, Long> {

	@Query(value="select * from m_user ",nativeQuery = true)
	List<userModel>userorder();
	
	@Query(value = "select id, biodata_id, role_id, email, password,"
			+ " login_attempt, is_locked, last_login from m_user where is_delete=false", nativeQuery = true)
	List<Map<String, Object>>list_User();
	
	@Query(value = "select id from m_user order by id desc limit 1", nativeQuery = true)
	int selectById();
	
	@Query(value = "select * from m_user where id= :id limit 1", nativeQuery = true)
	userModel userbyid(long id);
	
	@Modifying
	@Query(value = "update m_user set is_delete=true where id= :id ", nativeQuery = true)
	int deleteuser(long id);
	
	@Modifying
	@Query(value = "update m_user set created_by= :id where id= :id", nativeQuery = true)
	int setDefaultCreatedBy(long id);
	
	//mengecek email sudah ada atau blm
	@Query(value = "select coalesce(sum(id),0) from m_user where email = :email", nativeQuery = true)
	int cek_Email(String email);
	
	@Query(value=" select b.Fullname as Fullname from m_user u join m_biodata b on u.biodata_id=b.id where email= :em  and password = :ps limit 1 ", nativeQuery = true)
	String ceklogin(String em, String ps);
}
