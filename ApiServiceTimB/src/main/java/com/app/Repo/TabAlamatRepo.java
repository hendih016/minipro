package com.app.Repo;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.app.Model.TabAlamatModel;

@Transactional
public interface TabAlamatRepo extends JpaRepository<TabAlamatModel, Long> {

	@Query(value = "select * from m_biodata_address where is_delete = false order by id", nativeQuery = true)
	List<TabAlamatModel> listTabAlamatActive();
	
	@Query(value = "select * from m_biodata_address where id = :id limit 1",nativeQuery = true)
	TabAlamatModel tabalamatbyid(long id);
	
	@Modifying
	@Query(value = "update m_biodata_address set is_delete = true where id =:id",nativeQuery = true)
	int deleteTabAlamat(long id);
	
	@Query(value = "select id, label,"
			+ "address,"
			+ "recipient,"
			+ "recipient_phone_number as recipientphonenumber"
			+ " from m_biodata_address where lower(address) like lower(concat('%',:a,'%')) or lower(recipient) like lower(concat('%',:a,'%'))  order by id", nativeQuery = true)
	List<Map<String, Object>> searchTabAlamat(String a);
}
