package com.app.Repo;

import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.app.Model.DoctorModel;

@Transactional
public interface DoctorRepo extends JpaRepository<DoctorModel, Long>{
	
	@Query(value = "SELECT d.id as id, b.fullname, s.name as specname, mf.name as officename, (\r\n"
			+ "Select EXTRACT(YEAR FROM now())-TO_NUMBER(end_year,'9999') as tahun from m_doctor_education WHERE id=D.ID AND is_last_education = true\r\n"
			+ ") FROM m_doctor d\r\n"
			+ "JOIN m_biodata b ON d.biodata_id = b.id\r\n"
			+ "JOIN m_doctor_education de ON d.id = de.doctor_id\r\n"
			+ "JOIN t_current_doctor_specialization cds ON d.id = cds.doctor_id\r\n"
			+ "JOIN m_specialization s ON cds.specialization_id = s.id\r\n"
			+ "JOIN t_doctor_office dof ON d.id = dof.doctor_id\r\n"
			+ "JOIN m_medical_facility mf ON dof.medical_facility_id = mf.id", 
			nativeQuery = true)
	List<Map<Long, Object>> listDoctor();
	
}
