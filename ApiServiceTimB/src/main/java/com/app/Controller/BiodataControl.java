package com.app.Controller;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.Model.BiodataModel;
import com.app.Repo.BiodataRepo;

import io.swagger.v3.oas.annotations.parameters.RequestBody;

@RestController
public class BiodataControl {
	
	@Autowired
	private BiodataRepo br;
	
	@GetMapping("list")
	public List<Map<String, Object>>listBiodata(){
		
		return br.listBiodata();
	}
	
	@PostMapping("post")
	public void post(@RequestBody BiodataModel bm)
	{
		bm.setC_on(LocalDateTime.now());
		bm.setC_by((long) 0);
		br.save(bm);
	}
	
	@GetMapping("idbaru")
	public int newid(){
		return br.selectById();
	}
	
}
 