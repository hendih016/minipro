package com.app.Controller;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.Model.tokenModel;
import com.app.Repo.tokenRepo;
import com.app.com.MailConfig;



@RestController
@RequestMapping("api/token")
@CrossOrigin(origins="*") 
public class TokenControl {

	@Autowired
	private tokenRepo tr;
	
	@Autowired
	private MailConfig mc;
	
	@GetMapping("list")
	public List<tokenModel>tokeorder(){
		return tr.tokeorder();
	}
	
	@GetMapping("list1")
	public List<Map<String, Object>> list(){
		return tr.listToken();
	}
	
	//buat token by email
	@PostMapping("post")
	public void postToken(@RequestBody tokenModel tm) {
		Random rand =  new Random();
		int token = rand.nextInt(1000);
		tm.setToken(String.valueOf(token));
		tm.setExpired(LocalDateTime.now().plusMinutes(10));
		tm.setIs_expired(false);
		tm.setUsed("user create account");
		tm.setCreated_by(0);
		tm.setCreated_on(LocalDateTime.now());
		tr.save(tm);

		
	}
	
	//mengecek id dan token
	@GetMapping("cektoken/{id}/{token}")
	public int cekToken(@PathVariable int id ,@PathVariable String token){
		return tr.cek_Token(id,token);
	}
	
	@GetMapping("gettoken/{email}")
	public String getToken(@PathVariable String email){
		String token = tr.sendToken(email);
		sendToken(email, token);
		return tr.selectToken(email);
	}
	//kirim email
	public void sendToken(@PathVariable String email, @PathVariable String token){
		mc.kirim(email, "Verifikasi token", "Token "+token);
	}
	//cek expired token
		@GetMapping("cekvalid/{id}")
		public String cekValid(@PathVariable int id){
			return tr.cekValid(id);
		}
		

		
	
}
