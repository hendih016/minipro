package com.app.Controller;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import javax.websocket.OnError;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.Model.LevelLokasiModel;
import com.app.Repo.LevelLokasiRepo;

@RestController
@RequestMapping("api/levellokasi")
@CrossOrigin(origins = "*")
public class LevelLokasiControl {

	@Autowired
	private LevelLokasiRepo lr;
	
	public int deleteValid = 0;
	
	@GetMapping("list")
	public List<LevelLokasiModel> listLevelLokasi() {
		return lr.listLevelLokasi();
	}
	
	@GetMapping("list/{name}")
	public String levelLokasiByName(@PathVariable String name) {
		return lr.listLevLokByName(name);
	}

	@GetMapping("countlok/{id}")
	public int countLokasiById(@PathVariable Long id) {
		deleteValid = lr.countLokById(id);
		return lr.countLokById(id);
	}

	@GetMapping("LevLokById/{id}")
	public LevelLokasiModel LevLokById(@PathVariable Long id) {		
		return lr.LevLokById(id);
	}
	
	@Valid
	@PostMapping("add")
	public void addProduct(@RequestBody LevelLokasiModel lm) {
		lr.addLevLok(lm.getId(), lm.getName(), lm.getAbbr(), lm.getC_by());
	}
	
	@DeleteMapping("delete")
	public void deleteLevLok(@RequestBody LevelLokasiModel lm) {
		LocalDateTime today = LocalDateTime.now();

		if (deleteValid == 0) {
			lm.setD_on(today);
			lm.setD_by(1);
			
			lr.deleteLocLev(lm.getId());
		} else {
			System.out.println("Data masih dipakai");
		}
	}

	@PutMapping("update")
	public void editLevLok(@RequestBody LevelLokasiModel lm) {
		LocalDateTime today = LocalDateTime.now();
		lm.setM_on(today);
		lm.setM_by(1);

		lr.save(lm);
	}

}
