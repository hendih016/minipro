package com.app.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.Model.bankModel;
import com.app.Repo.bankRepo;




@RestController
@RequestMapping("api/bank")
@CrossOrigin(origins="*")
public class bankControl {

	@Autowired
	private bankRepo br;
	

	@GetMapping("list")
	public List<bankModel>bankorder(){
		return br.bankorder();
	}
	
	@GetMapping("list/{id}")
	public bankModel bankById(@PathVariable long id) {
		return br.bankById(id);
	}
	
	@PostMapping("add")
	public void addBank(@RequestBody bankModel bm) {
		br.save(bm);
	}
	
	@GetMapping("listbyName/{nm}")
	public List<bankModel> bankByNama(@PathVariable String nm){
		return br.bankByNama(nm);
	}
	
	@PutMapping("edit")
	public void editBank(@RequestBody bankModel bm) {
		br.save(bm);
	}
	
	@DeleteMapping("delete")
	public void deleteBank(@RequestBody bankModel bm) {
		br.deleteBank(bm.getId());
	}
	

}
