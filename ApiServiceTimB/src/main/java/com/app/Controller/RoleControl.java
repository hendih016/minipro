package com.app.Controller;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.Model.roleModel;
import com.app.Repo.roleRepo;



@RestController
@RequestMapping("api/role")
@CrossOrigin(origins="*")
public class RoleControl {
@Autowired
private roleRepo rr;

@GetMapping("list")
public List<Map<String, Object>>roleorder(){
	return rr.roleorder();
}

@PostMapping("add")
public void addrole( @RequestBody roleModel rm) {
	
	LocalDateTime today = LocalDateTime.now();
	rm.setCreated_on(today);
	rm.setCreated_by(1);
	rr.save(rm);
}

@GetMapping("list/{id}")
public roleModel roleById(@PathVariable long id){
	return rr.rolebyid(id);
}

@PutMapping("update")
public void updaterole(@RequestBody roleModel rm) {
	LocalDateTime today = LocalDateTime.now();
	rm.setModified_on(today);
	rm.setModified_by(1);
	rr.save(rm);
}

@DeleteMapping("delete")
public void deleterole(@RequestBody roleModel rm){
	LocalDateTime today = LocalDateTime.now();
	rm.setDeleted_on(today);
	rm.setModified_by(1);
	rr.deleterole(rm.getId());
}



}
