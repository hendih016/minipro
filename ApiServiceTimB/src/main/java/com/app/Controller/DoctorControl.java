package com.app.Controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.Repo.DoctorRepo;

@RestController
@RequestMapping("api/doctor")
@CrossOrigin(origins = "*")
public class DoctorControl {

	@Autowired
	private DoctorRepo dr;
		
	@GetMapping("list")
	public List<Map<Long, Object>> listDoctor() {
		return dr.listDoctor();
	}
}
