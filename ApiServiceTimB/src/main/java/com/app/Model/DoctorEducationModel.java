package com.app.Model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "m_doctor_education")
public class DoctorEducationModel {
	@Id	
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;

	@Column(name = "doctor_id")
	private long doctorId;

	@Column(name = "education_level_id")
	private long educationLevelId;

	@Size(max = 100)
	@Column(name = "institution_name")
	private String institutionName;

	@Size(max = 100)
	@Column(name = "major")
	private String major;

	@Size(max = 4)
	@Column(name = "start_year")
	private String startYear;

	@Size(max = 4)
	@Column(name = "end_year")
	private String endYear;

	@Column(name = "is_last_education", columnDefinition = "boolean default false")
	private Boolean isLastEducation;

	@NotNull
	@Column(name = "created_by", columnDefinition = "")
	private Long c_by;

	@NotNull
	@Column(name = "created_on", columnDefinition = "")
	private LocalDateTime c_on;
	
	@Column(name = "modified_by", columnDefinition = "bigint default 0")
	private Long m_by;

	@Column(name = "modified_on", columnDefinition = "timestamp default NOW()")
	private LocalDateTime m_on;
	
	@Column(name = "deleted_by", columnDefinition = "bigint default 0")
	private Long d_by;

	@Column(name = "deleted_on", columnDefinition = "timestamp default NOW()")
	private LocalDateTime d_on;

	@Column(name = "is_delete", columnDefinition = "boolean default false")
	private Boolean is_del;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(long doctorId) {
		this.doctorId = doctorId;
	}

	public long getEducationLevelId() {
		return educationLevelId;
	}

	public void setEducationLevelId(long educationLevelId) {
		this.educationLevelId = educationLevelId;
	}

	public String getInstitutionName() {
		return institutionName;
	}

	public void setInstitutionName(String institutionName) {
		this.institutionName = institutionName;
	}

	public String getMajor() {
		return major;
	}

	public void setMajor(String major) {
		this.major = major;
	}

	public String getStartYear() {
		return startYear;
	}

	public void setStartYear(String startYear) {
		this.startYear = startYear;
	}

	public String getEndYear() {
		return endYear;
	}

	public void setEndYear(String endYear) {
		this.endYear = endYear;
	}

	public Boolean getIsLastEducation() {
		return isLastEducation;
	}

	public void setIsLastEducation(Boolean isLastEducation) {
		this.isLastEducation = isLastEducation;
	}

	public Long getC_by() {
		return c_by;
	}

	public void setC_by(Long c_by) {
		this.c_by = c_by;
	}

	public LocalDateTime getC_on() {
		return c_on;
	}

	public void setC_on(LocalDateTime c_on) {
		this.c_on = c_on;
	}

	public Long getM_by() {
		return m_by;
	}

	public void setM_by(Long m_by) {
		this.m_by = m_by;
	}

	public LocalDateTime getM_on() {
		return m_on;
	}

	public void setM_on(LocalDateTime m_on) {
		this.m_on = m_on;
	}

	public Long getD_by() {
		return d_by;
	}

	public void setD_by(Long d_by) {
		this.d_by = d_by;
	}

	public LocalDateTime getD_on() {
		return d_on;
	}

	public void setD_on(LocalDateTime d_on) {
		this.d_on = d_on;
	}

	public Boolean getIs_del() {
		return is_del;
	}

	public void setIs_del(Boolean is_del) {
		this.is_del = is_del;
	}

}
