package com.app.Model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "m_location_level")
public class LevelLokasiModel {
	
	//Jangan lupa masukin validation di repo
	
	@Id	
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;
	
	@Column(name = "name", columnDefinition = "varchar (50) unique")
	private String name;
	
	@Size(max = 50)
	@Column(name = "abbreviation")
	private String abbr;
	
	@NotNull
	@Column(name = "created_by", columnDefinition = "")
	private Long c_by;

	@NotNull
	@Column(name = "created_on", columnDefinition = "")
	private LocalDateTime c_on;
	
	@Column(name = "modified_by", columnDefinition = "bigint default 0")
	private Long m_by;

	@Column(name = "modified_on", columnDefinition = "timestamp default NOW()")
	private LocalDateTime m_on;
	
	@Column(name = "deleted_by", columnDefinition = "bigint default 0")
	private Long d_by;

	@Column(name = "deleted_on", columnDefinition = "timestamp default NOW()")
	private LocalDateTime d_on;

	@Column(name = "is_delete", columnDefinition = "boolean default false")
	private Boolean is_del;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAbbr() {
		return abbr;
	}

	public void setAbbr(String abbr) {
		this.abbr = abbr;
	}

	public LocalDateTime getC_on() {
		return c_on;
	}

	public void setC_on(LocalDateTime c_on) {
		this.c_on = c_on;
	}

	public long getC_by() {
		return c_by;
	}

	public void setC_by(long c_by) {
		this.c_by = c_by;
	}

	public long getM_by() {
		return m_by;
	}

	public void setM_by(long m_by) {
		this.m_by = m_by;
	}

	public LocalDateTime getM_on() {
		return m_on;
	}

	public void setM_on(LocalDateTime m_on) {
		this.m_on = m_on;
	}

	public long getD_by() {
		return d_by;
	}

	public void setD_by(long d_by) {
		this.d_by = d_by;
	}

	public LocalDateTime getD_on() {
		return d_on;
	}

	public void setD_on(LocalDateTime d_on) {
		this.d_on = d_on;
	}

	public Boolean getIs_del() {
		return is_del;
	}

	public void setIs_del(Boolean is_del) {
		this.is_del = is_del;
	}
	
}
