package com.app.Model;

import java.sql.Date;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "m_customer")
public class CustomerModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;
	
	@Column(name = "biodata_id")
	private  long biodata_id;
	
	@Column(name = "dob")
	private Date  dob;
	
	@Column(name = "gender")
	private String gender;
	
	@Column(name = "blood_group_id")
	private  long blood_group_id;
	
	@Column(name = "rhesus_type")
	private  String rhesus_type;
	
	@Column(name = "height")
	private double height;
	
	@Column(name = "weight")
	private double weight;
	
	@Column(name = "created_by")
	private long createdBy;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getBiodata_id() {
		return biodata_id;
	}

	public void setBiodata_id(long biodata_id) {
		this.biodata_id = biodata_id;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public long getBlood_group_id() {
		return blood_group_id;
	}

	public void setBlood_group_id(long blood_group_id) {
		this.blood_group_id = blood_group_id;
	}

	public String getRhesus_type() {
		return rhesus_type;
	}

	public void setRhesus_type(String rhesus_type) {
		this.rhesus_type = rhesus_type;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public long getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(long createdBy) {
		this.createdBy = createdBy;
	}

	public LocalDateTime getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(LocalDateTime createdOn) {
		this.createdOn = createdOn;
	}

	public long getModifedBy() {
		return modifedBy;
	}

	public void setModifedBy(long modifedBy) {
		this.modifedBy = modifedBy;
	}

	public LocalDateTime getModifedOn() {
		return modifedOn;
	}

	public void setModifedOn(LocalDateTime modifedOn) {
		this.modifedOn = modifedOn;
	}

	public long getDeletedBy() {
		return deletedBy;
	}

	public void setDeletedBy(long deletedBy) {
		this.deletedBy = deletedBy;
	}

	public LocalDateTime getDeletedOn() {
		return deletedOn;
	}

	public void setDeletedOn(LocalDateTime deletedOn) {
		this.deletedOn = deletedOn;
	}

	public boolean isDelete() {
		return delete;
	}

	public void setDelete(boolean delete) {
		this.delete = delete;
	}

	@Column(name = "created_on")
	private LocalDateTime createdOn;
	
	@Column(name = "modifed_by")
	private long modifedBy;
	
	@Column(name = "modifed_on")
	private LocalDateTime modifedOn;
	
	@Column(name = "deleted_by")
	private long deletedBy;
	
	@Column(name = "deleted_on")
	private LocalDateTime deletedOn;
	
	@Column(name = "is_delete", columnDefinition = "boolean default false")
	private boolean delete ;
	
}
