package com.app.Model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "m_medical_facility")
public class MedicalFacilityModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private long id;
	
	@Size(max = 50)
	@Column(name = "name")
	private String name;

	@Column(name = "medical_facility_category_id")
	private long medicalFacilityCategoryId;	

	@Column(name = "location_id")
	private long locationId;	

	@Column(name = "full_address")
	private String fullAddress;	

	@Size(max = 100)
	@Column(name = "email")
	private String email;

	@Size(max = 10)
	@Column(name = "phone_code")
	private String phoneCode;

	@Size(max = 15)
	@Column(name = "phone")
	private String phone;

	@Size(max = 15)
	@Column(name = "fax")
	private String fax;
	
	@NotNull
	@Column(name = "created_by", columnDefinition = "")
	private Long c_by;

	@NotNull
	@Column(name = "created_on", columnDefinition = "")
	private LocalDateTime c_on;
	
	@Column(name = "modified_by", columnDefinition = "bigint default 0")
	private Long m_by;

	@Column(name = "modified_on", columnDefinition = "timestamp default NOW()")
	private LocalDateTime m_on;
	
	@Column(name = "deleted_by", columnDefinition = "bigint default 0")
	private Long d_by;

	@Column(name = "deleted_on", columnDefinition = "timestamp default NOW()")
	private LocalDateTime d_on;

	@Column(name = "is_delete", columnDefinition = "boolean default false")
	private Boolean is_del;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getMedicalFacilityCategoryId() {
		return medicalFacilityCategoryId;
	}

	public void setMedicalFacilityCategoryId(long medicalFacilityCategoryId) {
		this.medicalFacilityCategoryId = medicalFacilityCategoryId;
	}

	public long getLocationId() {
		return locationId;
	}

	public void setLocationId(long locationId) {
		this.locationId = locationId;
	}

	public String getFullAddress() {
		return fullAddress;
	}

	public void setFullAddress(String fullAddress) {
		this.fullAddress = fullAddress;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneCode() {
		return phoneCode;
	}

	public void setPhoneCode(String phoneCode) {
		this.phoneCode = phoneCode;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public Long getC_by() {
		return c_by;
	}

	public void setC_by(Long c_by) {
		this.c_by = c_by;
	}

	public LocalDateTime getC_on() {
		return c_on;
	}

	public void setC_on(LocalDateTime c_on) {
		this.c_on = c_on;
	}

	public Long getM_by() {
		return m_by;
	}

	public void setM_by(Long m_by) {
		this.m_by = m_by;
	}

	public LocalDateTime getM_on() {
		return m_on;
	}

	public void setM_on(LocalDateTime m_on) {
		this.m_on = m_on;
	}

	public Long getD_by() {
		return d_by;
	}

	public void setD_by(Long d_by) {
		this.d_by = d_by;
	}

	public LocalDateTime getD_on() {
		return d_on;
	}

	public void setD_on(LocalDateTime d_on) {
		this.d_on = d_on;
	}

	public Boolean getIs_del() {
		return is_del;
	}

	public void setIs_del(Boolean is_del) {
		this.is_del = is_del;
	}
	
}
